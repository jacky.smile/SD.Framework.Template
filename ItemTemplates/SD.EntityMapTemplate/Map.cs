﻿using SD.Common.PoweredByLee;

namespace $rootnamespace$
{
	/// <summary>
	/// XXX相关映射工具类
	/// </summary>
	public static class $safeitemrootname$
	{
		#region # 示例 —— static OrderInfo ToDTO(this Order order)
		/// <summary>
		/// 订单映射
		/// </summary>
		public static OrderInfo ToDTO(this Order order)
		{
			OrderInfo orderInfo = Transform<Order, OrderInfo>.Map(order);

			return orderInfo;
		}
		#endregion
	}
}
