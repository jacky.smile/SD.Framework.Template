﻿using System;

namespace $rootnamespace$
{
	/// <summary>
	/// XXX领域服务实现
	/// </summary>
	public class $safeitemrootname$ : I$safeitemrootname$
	{
		#region # 字段及依赖注入构造器

		/// <summary>
		/// 仓储中介者
		/// </summary>
		private readonly RepositoryMediator _repMediator;

		/// <summary>
		/// 依赖注入构造器
		/// </summary>
		/// <param name="repMediator">仓储中介者</param>
		public $safeitemrootname$(RepositoryMediator repMediator)
		{
			this._repMediator = repMediator;
		}

		#endregion
	}
}
