﻿namespace $safeprojectname$.Domain.IDomainServices
{
    /// <summary>
    /// 编号领域服务接口
    /// </summary>
    public interface INumberService
    {
        #region 示例：生成示例编号 —— string GenerateExampleNo()
        /// <summary>
        /// 示例：生成示例编号
        /// </summary>
        /// <returns>示例编号</returns>
        string GenerateExampleNo();
        #endregion
    }
}
