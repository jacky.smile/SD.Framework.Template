﻿using SD.IdentitySystem;
using SD.Infrastructure.Repository.EntityFramework;
using SD.Infrastructure.RepositoryBase;

namespace $safeprojectname$.Repository.Base
{
    /// <summary>
    /// 数据初始化器实现
    /// </summary>
    public class DataInitializer : IDataInitializer
    {
        /// <summary>
        /// 初始化基础数据
        /// </summary>
        public void Initialize()
        {
            //注册获取用户信息事件
            EFUnitOfWorkProvider.GetLoginInfo += () => Membership.LoginInfo;
    }
    }
}
