using System.Data.Entity.Migrations;
using RepositoryTemp.Base;
using SD.Infrastructure.RepositoryBase;
using SD.IOC.Core.Mediator;

namespace RepositoryTemp.Migrations
{
    internal sealed class Configuration : DbMigrationsConfiguration<DbSession>
    {
        public Configuration()
        {
            this.AutomaticMigrationsEnabled = true;
            this.AutomaticMigrationDataLossAllowed = true;
        }

        protected override void Seed(DbSession context)
        {
            //��ʼ������
            IDataInitializer initializer = ResolveMediator.Resolve<IDataInitializer>();
            initializer.Initialize();
        }
    }
}
