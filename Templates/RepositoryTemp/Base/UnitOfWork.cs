﻿using DomainTemp.IRepositories;
using SD.Infrastructure.Repository.EntityFramework;

namespace RepositoryTemp.Base
{
    /// <summary>
    /// 单元事务 - 请自行改名
    /// </summary>
    public sealed class UnitOfWork : EFUnitOfWorkProvider, IUnitOfWorkConcrete
    {

    }
}
